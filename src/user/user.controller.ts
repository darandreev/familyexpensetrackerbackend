import { Controller, Post, Body, HttpException, Get, Query, Put, Patch } from '@nestjs/common';
import { UserService } from './user.service';
import { DeepPartial, QueryFailedError } from 'typeorm';
import { User } from './user.entity';

@Controller('user')
export class UserController {

    constructor(private userService: UserService) {}

    @Post('/signup')
    public async create(@Body() data: DeepPartial<User>): Promise<User> {
        try {
            return await this.userService.create(data);
        } catch (error) {
            if (error instanceof QueryFailedError) {
                throw new HttpException(error.message, 500);
            } else {
                throw new HttpException({ error }, 500);
            }
        }
    }

    @Get()
    public async getUser(@Query('userId') userId: string): Promise<User> {
        return await this.userService.findOneById(userId);
    }

    @Patch()
    public async updateUserAmount(@Query('userId') userId: string, @Query('amount') amount: number): Promise<User> {
        return await this.userService.updateUserAmount(userId, amount);
    }
}
