import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository, DeepPartial, Entity } from 'typeorm';
import { LoginUserDto } from './dto/login-user.dto';
import { passwordHash } from '@app/helpers/password.hash';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) protected readonly userRepository: Repository<User>,
  ) {}

  public async login(credentials: LoginUserDto) {
    const user = await this.userRepository.findOne({
      username: credentials.username,
      password: passwordHash(credentials.password),
    });

    if (!user) {
      throw new NotFoundException('User not found');
    }

    return user;
  }

  public async create(data: DeepPartial<User>): Promise<User> {
    const entity: User = this.userRepository.create(data);
    return entity.save();
  }

  public async updateUserAmount(userId: string, amount: number): Promise<User> {
    const user = await this.findOneById(userId);
    user.amount = amount;
    return user.save();

  }

  public async findOneById(id: string): Promise<User> {
    const user = await this.userRepository.findOneOrFail(id);

    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }
}
