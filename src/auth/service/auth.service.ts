import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { UserService } from '@app/user/user.service';
import { LoginUserDto } from '@app/user/dto/login-user.dto';
import { JwtPayload } from '@app/auth/interface/jwt.payload.interface';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserService) { }

  async createToken(credentials: LoginUserDto) {
    const user = await this.userService.login(credentials);
    const expiresIn = '1d';
    const accessToken = jwt.sign({ id: user.id, username: user.username }, 'pe4enakoko6ka', {
      expiresIn,
    });
    delete user.password;
    return {
      expiresIn,
      accessToken,
      user,
    };
  }

  async verifyToken(token: string) {
    return new Promise(resolve => {
      token = token.replace(/^Bearer\s/, '');
      jwt.verify(token, 'pe4enakoko6ka', decoded => resolve(decoded));
    });
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    return await this.userService.findOneById(payload.id);
  }
}
