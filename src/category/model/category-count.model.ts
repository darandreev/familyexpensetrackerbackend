export interface ICategory {
  name: string;
  amount: number;
  count: number;
}
export interface IDashboard {
  categories: ICategory[];
  totalMonthlyAmount: number;
}
