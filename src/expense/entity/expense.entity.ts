import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  BaseEntity,
} from 'typeorm';
import { Category } from '../../category/entity/category.entity';
import { User } from '../../user/user.entity';

@Entity()
export class Expense extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar' })
  title: string;

  @Column({ type: 'double' })
  amount: number;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  date: Date | null;

  @Column({ type: 'varchar' })
  description: string;

  @Column({ type: 'varchar', default: null })
  categoryId?: string;

  categoryName?: string;

  @ManyToOne(type => Category)
  @JoinColumn({ name: 'categoryId', referencedColumnName: 'id' })
  category?: Category;

  @Column({ type: 'varchar', nullable: false })
  userId: string;

  @ManyToOne(type => User)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  user?: User;
}
